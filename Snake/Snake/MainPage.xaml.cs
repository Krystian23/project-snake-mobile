﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using SQLite;
using System.IO;
using System.Threading;

namespace Snake
{
    class Circle
    {
        ///<summary>
        ///Klasa odpowiadająca za stworzenie obiektów koła, posiada 3 właściwości x,y oraz radius
        ///</summary>

        public int X { get; set; }
        public int Y { get; set; }

        public int radius { get; set; }

        public Circle()
        {
            X = -100;
            Y = -100;
            radius = 10;
        }
    }
    public partial class MainPage : ContentPage
    {
        Random rand = new Random();
        private List<Circle> Snake = new List<Circle>();
        private Circle food = new Circle { X = 450, Y = 550 };

        int minpos = 200;
        int maxpos_x = 900;
        int maxpos_y = 1200;
        int score = 0;
        bool device_timer = true;
        string direction = "up";
        string result;

        public MainPage()
        {
            InitializeComponent();
            if(Device.RuntimePlatform== Device.Android)
            {
                NavigationPage.SetHasBackButton(this, false);
            }

            Circle head = new Circle { X = 450, Y = 600 };
            Snake.Add(head);

            Device.StartTimer(new TimeSpan(0, 0, 0), () =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    // interact with UI elements
                });
                timer();
                if (device_timer == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }
        public void timer()
        {
            ///<summary>
            /// metoda odpowiadająca za odświeżanie pozycji węża, sprawdza również kolizję pomiędzy obiektami
            ///</summary>


            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    switch (direction)
                    {
                        case "left":
                            Snake[i].X = Snake[i].X - 10;
                            Thread.Sleep(100);
                            break;
                        case "right":
                            Snake[i].X = Snake[i].X + 10;
                            Thread.Sleep(100);
                            break;
                        case "down":
                            Snake[i].Y = Snake[i].Y + 10;
                            Thread.Sleep(100);
                            break;
                        case "up":
                            Snake[i].Y = Snake[i].Y - 10;
                            Thread.Sleep(100);
                            break;
                    }
                    
                    if (Snake[i].X <= minpos)
                    {
                        Snake[i].X = maxpos_x - 20;
                    }
                    if (Snake[i].X >= maxpos_x)
                    {
                        Snake[i].X = minpos + 20;
                    }
                    if (Snake[i].Y <= minpos)
                    {
                        Snake[i].Y = maxpos_y - 20;
                    }
                    if (Snake[i].Y >= maxpos_y)
                    {
                        Snake[i].Y = minpos + 20;
                    }
                    if (Snake[i].X == food.X && Snake[i].Y == food.Y)
                    {
                        IncreaseScore();
                    }

                }
                else
                {
                    Snake[i].X = Snake[i - 1].X;
                    Snake[i].Y = Snake[i - 1].Y;
                }
            }

            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    for (int j = 1; j < Snake.Count; j++)
                    {
                        if (Snake[i].X == Snake[j].X && Snake[i].Y == Snake[j].Y)
                        {
                            EndGame();
                        }

                    }
                }
            }
            canvasView.InvalidateSurface();
        }
        ///<summary>
        ///Event pochodzący z SkiaSharp, odpowiada za rysowanie obiektów
        ///</summary>
        private void canvasView_PaintSurface(object sender, SkiaSharp.Views.Forms.SKPaintSurfaceEventArgs e)
        {

            
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;
            var paint1 = new SKPaint
            {
                Color = SKColors.Black,
                Style = SKPaintStyle.Fill
            };

            var paint_food = new SKPaint
            {
                Color = SKColors.Green,
                Style = SKPaintStyle.Fill
            };

            var paint2 = new SKPaint
            {
                Color = SKColors.Red,
                Style = SKPaintStyle.Fill
            };
            var background = new SKPaint
            {
                Color = SKColors.Gray,
                Style = SKPaintStyle.Fill
            };
            int width = e.Info.Width;
            int height = e.Info.Height;
            canvas.DrawRect(minpos, minpos, maxpos_x - minpos, maxpos_y - minpos, background);
            
            SKPaint snakeColour;
            for (int i = 0; i < Snake.Count; i++)
            {
                
                if (i == 0)
                {
                    snakeColour = paint2;
                }
                else
                {
                    snakeColour = paint1;
                }
                canvas.DrawCircle(Snake[i].X, Snake[i].Y, Snake[i].radius, snakeColour);
            }
            canvas.DrawCircle(food.X, food.Y, food.radius, paint_food);
        }
        ///<summary>
        ///Metoda dodaje nam punkty oraz powiększa węża
        ///</summary>
        private void IncreaseScore()
        {

            
            score += 1;
            switch (direction)
            {
                case "left":
                    Circle body = new Circle
                    {
                        X = Snake[Snake.Count - 1].X + 10,
                        Y = Snake[Snake.Count - 1].Y
                    };

                    Snake.Add(body);
                    break;
                case "right":
                    Circle body2 = new Circle
                    {
                        X = Snake[Snake.Count - 1].X - 10,
                        Y = Snake[Snake.Count - 1].Y
                    };

                    Snake.Add(body2);
                    break;
                case "down":

                    Circle body3 = new Circle
                    {
                        X = Snake[Snake.Count - 1].X,
                        Y = Snake[Snake.Count - 1].Y - 10
                    };

                    Snake.Add(body3);
                    break;
                case "up":
                    Circle body4 = new Circle
                    {
                        X = Snake[Snake.Count - 1].X,
                        Y = Snake[Snake.Count - 1].Y + 10
                    };

                    Snake.Add(body4);
                    break;

            }
            var x = Math.Abs(rand.Next(minpos, maxpos_x)/10)*10;
            var y = Math.Abs(rand.Next(minpos, maxpos_y)/10)*10;
            food = new Circle { X = x, Y = y };
        }
        ///<summary>
        /// metoda odpowiadająca za wyświetlenie wyniku
        ///</summary>
        private void EndGame()
        {

            
            device_timer = false;
            GetName();

            System.Diagnostics.Debug.WriteLine("Twój wynik to:");
            System.Diagnostics.Debug.WriteLine(score);
            //Snake.Clear();
            //Circle head = new Circle { X = 450, Y = 600 };
            //Snake.Add(head); // adding the head part of the snake to the list
        }
        ///<summary>
        ///metoda odpowiadająca za pobieranie imienia po przegranej grze
        ///</summary>
        private async void GetName()
        {

            
            result = await DisplayPromptAsync("Podaj Imie", "");
            InsertDatabase();
            Navigation.PushAsync(new VewScore());
        }
        ///<summary>
        ///metoda odpowiadająca za dodawanie imienia oraz wyniku do bazy danych
        ///</summary>
        void InsertDatabase()
        {

            

            Score wynik = new Score()
            {
                Name = result,
                Score1 = score

            };
            using (SQLiteConnection conn = new SQLiteConnection(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Wyniki.db3")))
            {
                conn.CreateTable<Score>();
                int rowsAdded = conn.Insert(wynik);
            }
        }
        ///<summary>
        ///Metoda wywoływana jest po kliknięciu przycisku "Left", zmienia kierunek węża w lewo
        ///</summary>
        private void Left_Clicked(object sender, EventArgs e)
        {

            
            direction = "left";
        }
        ///<summary>
        ///Metoda wywoływana jest po kliknięciu przycisku "Up", zmienia kierunek węża w górę
        ///</summary>
        private void Up_Clicked(object sender, EventArgs e)
        {

            
            direction = "up";
        }
        ///<summary>
        ///Metoda wywoływana jest po kliknięciu przycisku "Down", zmienia kierunek węża w dół
        ///</summary>
        private void Down_Clicked(object sender, EventArgs e)
        {

            
            direction = "down";
        }
        ///<summary>
        ///Metoda wywoływana jest po kliknięciu przycisku "Right", zmienia kierunek węża w prawo
        ///</summary>
        private void Right_Clicked(object sender, EventArgs e)
        {
            
            direction = "right";
        }
        ///<summary>
        ///kontrola przycisku odpoiwadającego za zmianę okna
        ///</summary>
        private void WynikiClicked(object sender, EventArgs e)
        {

            
            Navigation.PushAsync(new VewScore());
        }
    }
}
