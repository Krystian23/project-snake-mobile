﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Snake
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VewScore : ContentPage
    {
        public VewScore()
        {
            InitializeComponent();
            if (Device.RuntimePlatform == Device.Android)
            {
                NavigationPage.SetHasBackButton(this, false);

            }
        }
        ///<summary>
        ///kontrolka przycisku odpoiwadająca za zmianę okna
        ///</summary>
        private void GameClicked(object sender, EventArgs e)
        {

            
            Navigation.PushAsync(new MainPage());
        }
        ///<summary>
        ///metoda odpowiadająca za wyświetlanie wyników po otworzeniu okna
        ///</summary>
        protected override void OnAppearing()
        {

            
            base.OnAppearing();

            using(SQLiteConnection conn = new SQLiteConnection(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Wyniki.db3")))
            {
                conn.CreateTable<Score>();
                var contacts = conn.Table<Score>().ToList();

                ScoreListView.ItemsSource = contacts;
            }
        }
    }
}