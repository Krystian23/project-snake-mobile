﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Snake
{
    ///<summary>
    ///tabela bazy danych
    ///</summary>
    public class Score
    {
        
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score1 { get; set; }

    }
    ///<summary>
    ///kontrola bazy danych
    ///</summary>
    public class Database
    {
        
        readonly SQLiteAsyncConnection _database;
        public Database(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Score>().Wait();

        }
        public Task<List<Score>> GetPeopleAsync()
        {
            return _database.Table<Score>().ToListAsync();

        }

        public Task<int> SavePersonAsync(Score score)
        {
            return _database.InsertAsync(score);
        }
        public Task<int> DeletePersonAsync(Score score)
        {
            return _database.DeleteAsync(score);
        }
        public Task<int> UpdatePersonaAsync(Score score)
        {
            return _database.UpdateAsync(score);
        }


    }
}
